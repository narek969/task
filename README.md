## About Project

For running the project we need to ...

1) Clone project from BitBucket repository

2) run 'composer install' command 

3) run 'cp .env.example .env' command 

4) run 'php artisan key:generate' command 

5) create DB and write DB name and password in .env file

6) run 'php artisan migrate:fresh --seed'

7) write email account info in .env for sending verification email

8) run 'php artisan serve'


I have created only Google Api for Socialite,and for Facebook we need to only create Facebook Api and write 'client_id','client_secret','redirect' parameters in the config/services.php.
