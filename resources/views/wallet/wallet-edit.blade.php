@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            {{ Session::get("message") }}
            <form action="{{ route('wallets.update',$wallet->id) }}" method="post" class="wallet-form">
                @csrf
                @method('put')
                <div class="form-group">
                    <label for="exampleInputEmail1">Name</label>
                    <input type="text" class="form-control" name="name" placeholder="Enter Name" value="{{ $wallet->name }}">
                    @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Wallet Type</label>
                    <select class="form-control" name="type" id="exampleFormControlSelect1">
                        <option selected>Choose...</option>
                        @foreach($walletTypes as $walletType)
                            <option value="{{ $walletType->id }}" @if($wallet->type === $walletType->id) selected @endif>{{ $walletType->name }}</option>
                        @endforeach
                    </select>
                    @error('type')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Balance</label>
                    <input type="number" name="balance" class="form-control" placeholder="Enter Balance" value="{{ $wallet->balance }}">
                    @error('balance')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection
