@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-end p-3">
            <a href="{{ route('wallets.create') }}" class="btn btn-success">Add Wallet</a>
        </div>
        <div class="row justify-content-center p-3">
                <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Type</th>
                        <th scope="col">Total Amount</th>
                        <th scope="col">Edit</th>
                        <th scope="col">Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach(auth()->user()->wallets as $wallet)
                        <tr>
                            <th scope="row">{{ $wallet->id }}</th>
                            <td>{{ $wallet->name }}</td>
                            <td>{{ $wallet->getType->name }}</td>
                            <td>{{ $wallet->balance }}</td>
                            <td><a href="{{ route('wallets.edit',$wallet->id) }}" class="btn btn-primary">Edit</a></td>
                            <td>
                                <form action="{{ route('wallets.destroy',$wallet->id) }}" method="post">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
        </div>
    </div>
@endsection
