@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <form action="{{ route('wallets.store') }}" method="post" class="wallet-form">
                @csrf
                <div class="form-group">
                    <label for="exampleInputEmail1">Name</label>
                    <input type="text" class="form-control" name="name" placeholder="Enter Name">
                    @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Wallet Type</label>
                    <select class="form-control" name="type" id="exampleFormControlSelect1">
                        <option selected>Choose...</option>
                        @foreach($walletTypes as $walletType)
                            <option value="{{ $walletType->id }}">{{ $walletType->name }}</option>
                        @endforeach
                    </select>
                    @error('type')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Balance</label>
                    <input type="number" name="balance" class="form-control" placeholder="Enter Balance">
                    @error('balance')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection
