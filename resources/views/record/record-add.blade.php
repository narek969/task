@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @if (session('message'))
                <div class="alert alert-danger">
                    {{ session('message') }}
                </div>
            @endif
        </div>
        <div class="row justify-content-center">
            <form action="{{ route('records.store') }}" method="post" class="wallet-form">
                @csrf
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Wallet</label>
                    <select class="form-control" name="wallet_id" id="exampleFormControlSelect1">
                        <option selected>Choose...</option>
                        @foreach(auth()->user()->wallets as $wallet)
                            <option value="{{ $wallet->id }}">{{ $wallet->name }}</option>
                        @endforeach
                    </select>
                    @error('wallet_id')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Type</label>
                    <select class="form-control" name="type" id="exampleFormControlSelect1">
                        <option selected>Choose...</option>
                        @foreach(Record::RECORD_TYPES as $key => $value)
                            <option value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>
                    @error('type')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Amount</label>
                    <input type="number" name="amount" class="form-control" placeholder="Enter Balance">
                    @error('amount')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection
