@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @if (session('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif
        </div>
        <div class="row justify-content-end p-3">
            <a href="{{ route('records.create') }}" class="btn btn-success">Add Record</a>
        </div>
        <div class="row justify-content-center p-3">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Wallet Name</th>
                    <th scope="col">Record Amount</th>
                    <th scope="col">Type</th>
                </tr>
                </thead>
                <tbody>
                @foreach(auth()->user()->records as $record)
                    <tr>
                        <th scope="row">{{ $record->id }}</th>
                        <td>{{ $record->wallet->name }}</td>
                        <td>{{ $record->amount }}</td>
                        <td>{{ Record::RECORD_TYPES[$record->type]}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
