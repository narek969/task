<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    protected $fillable = ['name','type','balance','user_id'];

    public function getType(){
        return $this->belongsTo(WalletType::class,'type');
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
