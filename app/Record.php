<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Record extends Model
{
    protected $fillable = ['wallet_id', 'amount','type'];

    const RECORD_TYPES = ['Debit','Credit'];

    public function wallet(){
        return $this->belongsTo(Wallet::class);
    }
}
