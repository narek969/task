<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WalletType extends Model
{
    public function wallets(){
        return $this->hasMany(Wallet::class,'type','id');
    }
}
