<?php

namespace App\Http\Middleware;

use Closure;

class WalletMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->user()->wallets->count() && $request->route()->getName()!=='wallets.create' && $request->route()->getName()!=='wallets.store'){
            return redirect()->route('wallets.create');
        }
        return $next($request);
    }
}
