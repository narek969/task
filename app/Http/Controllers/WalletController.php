<?php

namespace App\Http\Controllers;

use App\Wallet;
use App\WalletType;
use Illuminate\Http\Request;

class WalletController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('wallet.wallet-list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $walletTypes = WalletType::all();
        return view('wallet.wallet-add',compact('walletTypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $walletTypes = WalletType::all();

        $validatedData = $request->validate([
            'name' => ['required', 'unique:wallets','max:255'],
            'type' => ['required','in:'.$walletTypes->implode('id', ', ')],
            'balance' => ['required'],
        ]);

        $data = new Wallet($validatedData);

        auth()->user()->wallets()->save($data);

        return redirect()->route('wallets.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $wallet = Wallet::findOrFail($id);
        $walletTypes = WalletType::all();
        return view('wallet.wallet-edit',compact('wallet','walletTypes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $wallet = Wallet::findOrFail($id);
        $walletTypes = WalletType::all();

        $validatedData = $request->validate([
            'name' => ['sometimes','required', 'unique:wallets,id,' . $id,'max:255'],
            'type' => ['required','in:'.$walletTypes->implode('id', ', ')],
            'balance' => ['required'],
        ]);

        if (auth()->user()->is($wallet->user)) {
            $wallet->update($validatedData);
        }

        return redirect()->route('wallets.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $wallet = Wallet::findOrFail($id);
        if (auth()->user()->is($wallet->user)) {
            $wallet->delete();
            return redirect()->route('wallets.index');
        }
        return back();
    }
}
