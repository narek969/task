<?php

namespace App\Http\Controllers;

use App\Record;
use App\RecordType;
use App\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class RecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('record.record-list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('record.record-add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
           'wallet_id' => ['required:in:'.auth()->user()->wallets->implode('id', ', ')],
           'amount' => ['required'],
           'type' => ['required']
        ]);

        $wallet = Wallet::findOrFail($request->wallet_id);
        if (auth()->user()->is($wallet->user)) {
            if($validatedData['type'] == 0){
                if (auth()->user()->balance >= $request->amount) {
                    auth()->user()->balance -= $request->amount;
                    $wallet->balance += $request->amount;
                    auth()->user()->update();
                    $wallet->update();
                    Record::create($validatedData);
                }else{
                    return redirect()->back()->with('message', 'Insufficient funds.Not enough money on your account');
                }
            }elseif ($validatedData['type'] == 1){
                if ($wallet->balance >= $request->amount) {
                    $wallet->balance -= $request->amount;
                    auth()->user()->balance += $request->amount;
                    auth()->user()->update();
                    $wallet->update();
                    Record::create($validatedData);
                }else{
                    return redirect()->back()->with('message', 'Insufficient funds.Not enough money on your wallet');
                }
            }
        } else {
            return abort(403);
        }

        return redirect()->route('records.index')->with('message','Record created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
